<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller 
{
	public function __construct() {
		parent::__construct();
		$this->load->model('mdlpegawai');

		if($this->session->has_userdata('login') != 'success') {
			redirect(base_url('login'));
		}
	}

	public function index() {
		$getData = $this->mdlpegawai->getDataPegawai('tb_pegawai')->result();
		$data['tmpData'] = $getData;
		
		$this->load->view('tabel_pegawai', $data);
	}
	
	public function add() {
		$this->load->view('form_pegawai');
	}

	public function create() {
		$path = '../kepegawaian-ci/assets/images';
		$config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '100';
        $config['max_width'] = '1024';
		$config['max_height'] = '768';
		
		$this->load->library('upload', $config);
        $this->upload->initialize($config);

		if(!$this->upload->do_upload('foto')) {
			$error = array('error' => $this->upload->display_errors());
            print_r($error);
            $this->session->set_flashdata('msg_error', implode($error));
		} else {
			$nama_pegawai = $this->input->post('nama_pegawai');
			$jenis_kelamin = $this->input->post('jenis_kelamin');
			$pendidikan = $this->input->post('pendidikan');
			$no_telp = $this->input->post('no_telp');
            $file_name = $this->upload->data('file_name');

			$dataInsert = array(
				'nama_pegawai' => $nama_pegawai,
				'jenis_kelamin' => $jenis_kelamin,
				'pendidikan' => $pendidikan,
				'no_telp' => $no_telp,
				'foto' => $file_name
			);

			$this->mdlpegawai->insertPegawai('tb_pegawai', $dataInsert);
			$this->session->set_flashdata('message', 'Success Add');

			redirect(base_url('pegawai'));
		}
	}

	public function edit($id) {
		$where = array('id' => $id);
        $data['tmpEditPegawai'] = $this->mdlpegawai->editPegawai($where);

		$this->load->view('form_edit_pegawai', $data);
	}

	public function update() {
        $id = $this->input->post('id');
		$nama_pegawai = $this->input->post('nama_pegawai');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$pendidikan = $this->input->post('pendidikan');
		$no_telp = $this->input->post('no_telp');

		$dataUpdate = array(
			'nama_pegawai' => $nama_pegawai,
			'jenis_kelamin' => $jenis_kelamin,
			'pendidikan' => $pendidikan,
			'no_telp' => $no_telp
		);

        $where = array('id' => $id);
		$this->mdlpegawai->updatePegawai('tb_pegawai', $where, $dataUpdate);
		redirect(base_url('pegawai'));
	}

	public function delete($id) {
		$where = array('id' => $id);

		$this->mdlpegawai->deletePegawai('tb_pegawai', $where);
        redirect(base_url('pegawai'));
	}
}