<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
	public function __construct() {
		parent::__construct();
		$this->load->model('mdllogin');
	}
	
	public function index() {
		if($this->session->has_userdata('login') == 'success') {
			redirect(base_url('/'));
		}
		
		$this->load->view('login');
	}

	public function auth()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $pass = md5($password);

        $cek_admin = $this->mdllogin->authentication($username, $pass);

        if($cek_admin->num_rows() >= 1) 
        {
            foreach($cek_admin->result() as $data) {
                $sess_data['login'] = 'success';
                $this->session->set_userdata($sess_data);

                if($this->session->userdata('login') == 'success') {
                    redirect(base_url('/'));
                } else {
                    $this->session->set_flashdata('message', 'Invalid username or password, please check!');
                    return redirect($this->agent->referrer());
                }
            }
        }
        else 
        {
            $this->session->set_flashdata('message', 'Invalid username or password, please check!');
            redirect($this->agent->referrer());
        }
	}
	
	public function logout() {
        if($this->session->userdata('login') == 'success') {
            $this->session->sess_destroy();
        }

        redirect($this->agent->referrer());
    }
}