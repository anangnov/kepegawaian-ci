<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
	public function __construct() {
		parent::__construct();

		if($this->session->has_userdata('login') != 'success') {
			redirect(base_url('login'));
		}

		$this->load->model('mdlpegawai');
	}
	
	public function index() {
		
		$data['sd'] = $this->mdlpegawai->countPegawai('pendidikan', 'sd')->num_rows();
		$data['smp'] = $this->mdlpegawai->countPegawai('pendidikan', 'smp')->num_rows();
		$data['sma'] = $this->mdlpegawai->countPegawai('pendidikan', 'sma')->num_rows();
		$data['s1'] = $this->mdlpegawai->countPegawai('pendidikan', 's1')->num_rows();

		$data['laki_laki'] = $this->mdlpegawai->countPegawai('jenis_kelamin', 'laki-laki')->num_rows();
		$data['perempuan'] = $this->mdlpegawai->countPegawai('jenis_kelamin', 'perempuan')->num_rows();

		$this->load->view('dashboard', $data);
	}
}