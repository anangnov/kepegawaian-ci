<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MdlPegawai extends CI_Model
{
    public function getDataPegawai($table)
    {
        return $this->db->get($table);
    }

    public function insertPegawai($table, $data) {
        $this->db->insert($table, $data);
    }

    public function editPegawai($table, $where)
    {
        return $this->db->get_where($table, $where);
    }

    public function updatePegawai($table, $where, $data)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function deletePegawai($table, $where)
    {
        $this->db->delete($table, $where);
    }

    public function countPegawai($whereColumn, $where) {
        $query = $this->db->query("SELECT * FROM tb_pegawai WHERE $whereColumn = '$where'");
        return $query;
    }
}

?>
