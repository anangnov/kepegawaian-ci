<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('layout/meta') ?>
    <title>Kepegawaian - Data Pegawai</title>
    <?php $this->load->view('layout/css') ?>    
</head>
<body id="page-top">
  <div id="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php $this->load->view('layout/header') ?>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Data Pegawai</h1>   
            <a href="<?php echo base_url('pegawai/add') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Pegawai</a>         
          </div>
          <div class="row">
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Form Pegawai</h6>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama Pegawai</th>
                          <th>Pendidikan</th>
                          <th>Jenis Kelamin</th>
                          <th>No Telpon</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          $no = 1;
                          foreach ($tmpData as $key => $value) 
                          {
                        ?>
                        <tr>
                          <td><?php echo $no ?></td>
                          <td><?php echo $value->nama_pegawai ?></td>
                          <td style="text-transform: uppercase;"><?php echo $value->pendidikan ?></td>
                          <td><?php echo $value->jenis_kelamin ?></td>
                          <td><?php echo $value->no_telp ?></td>
                          <td>
                            <div class="d-flex">
                              <div class="pr-2">
                                <a href="<?php echo base_url('pegawai/edit/'.$value->id); ?>">
                                  <i class="fas fa-edit"></i>
                                </a>
                              </div>
                              <div>
                                <a href="<?php echo base_url('pegawai/delete/'.$value->id); ?>">
                                  <i class="fas fa-trash-alt"></i>
                                </a>
                              </div>
                            </div>
                          </td>
                        </tr>
                        <?php 
                          $no++;                        
                          } 
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <?php $this->load->view('layout/footer') ?>      

    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php $this->load->view('layout/js') ?>
  
</body>
</html>