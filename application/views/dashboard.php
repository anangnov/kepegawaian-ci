<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('layout/meta') ?>
    <title>Kepegawaian - Dashboard</title>
    <?php $this->load->view('layout/css') ?>    
</head>
<body id="page-top">
  <div id="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php $this->load->view('layout/header') ?>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>            
          </div>
          <div class="row">
            <div class="col-xl-6 col-lg-12">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Statistik Pendidikan Pegawai</h6>
                </div>
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                    <canvas id="myChartTwo"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-6 col-lg-12">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Statistik Jenis Kelamin</h6>
                </div>
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                    <canvas id="myChart"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <?php $this->load->view('layout/footer') ?>      

    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php $this->load->view('layout/js') ?>
  <script>
    var oilCanvas = document.getElementById("myChartTwo");

    Chart.defaults.global.defaultFontFamily = "Lato";
    Chart.defaults.global.defaultFontSize = 18;

    var oilData = {
        labels: [
            "SD",
            "SMP",
            "SMA",
            "S1"
        ],
        datasets: [
            {
                data: [<?php echo $sd ?>, <?php echo $smp ?>, <?php echo $sma ?>, <?php echo $s1 ?>],
                backgroundColor: [
                    "#FF6384",
                    "#63FF84",
                    "#84FF63",
                    "#8463FF"
                ]
            }]
    };

    var pieChart = new Chart(oilCanvas, {
      type: 'pie',
      data: oilData
    });
  </script>
  <script>
    var oilCanvas = document.getElementById("myChart");

    Chart.defaults.global.defaultFontFamily = "Lato";
    Chart.defaults.global.defaultFontSize = 18;

    var oilData = {
        labels: [
            "Laki-laki",
            "Perempuan"
        ],
        datasets: [
            {
                data: [<?php echo $laki_laki ?>, <?php echo $perempuan ?>],
                backgroundColor: [
                    "#7e8ea9",
                    "#e74a3b"
                ]
            }]
    };

    var pieChart = new Chart(oilCanvas, {
    type: 'pie',
    data: oilData
    });
  </script>
</body>
</html>