<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('layout/meta') ?>
    <title>Kepegawaian - Form Pegawai</title>
    <?php $this->load->view('layout/css') ?>    
</head>
<body id="page-top">
  <div id="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php $this->load->view('layout/header') ?>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Pegawai</h1>            
          </div>
          <form method="post" action="<?php echo base_url('pegawai/create'); ?>" enctype="multipart/form-data">
          <div class="row">
            <div class="col-xl-6 col-lg-12">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Form Pegawai</h6>
                </div>
                <div class="card-body">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="nama_pegawai" placeholder="Nama Pegawai" />
                  </div>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="no_telp" placeholder="No Telpon" />
                  </div>
                  <div class="input-group mb-3">
                    <select name="pendidikan" class="custom-select" id="inputGroupSelect01">
                      <option selected>Pilih Pendidikan</option>
                      <option value="sd">SD</option>
                      <option value="smp">SMP</option>
                      <option value="sma">SMA</option>
                      <option value="s1">S1</option>
                    </select>
                  </div>
                  <div class="input-group mb-3">
                    <select name="jenis_kelamin" class="custom-select" id="inputGroupSelect01">
                      <option selected>Pilih Jenis Kelamin</option>
                      <option value="laki-laki">Laki-laki</option>
                      <option value="perempuan">Perempuan</option>
                    </select>
                  </div>
                  <div class="mb-4">
                    <label>Pilih Gambar</label>
                    <div class="mt-1">
                      <input type="file" name="foto" />
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>

    <?php $this->load->view('layout/footer') ?>      

    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php $this->load->view('layout/js') ?>
  
</body>
</html>