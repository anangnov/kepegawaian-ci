<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('layout/meta') ?>
    <title>Kepegawaian - Form Pegawai</title>
    <?php $this->load->view('layout/css') ?>    
</head>
<body id="page-top">
  <div id="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php $this->load->view('layout/header') ?>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Pegawai</h1>            
          </div>
            <?php foreach ($tmpEditPegawai as $key => $value) 
                { 
            ?>
          <form method="post" action="<?php echo base_url('pegawai/update'); ?>">
          <div class="row">
            <div class="col-xl-6 col-lg-12">
              <input type="hidden" class="form-control" name="id" value="<?php echo $value->id ?>" />  
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Form Pegawai</h6>
                </div>
                <div class="card-body">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="nama_pegawai" value="<?php echo $value->nama_pegawai ?>" placeholder="Nama Pegawai" />
                  </div>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="no_telp" value="<?php echo $value->no_telp ?>" placeholder="No Telpon" />
                  </div>
                  <div class="input-group mb-3">
                    <select name="pendidikan" class="custom-select">
                    <?php 
                        if($value->pendidkan == 'sd') {
                            echo '
                            <option selected value="sd">SD</option>
                            <option value="smp">SMP</option>
                            <option value="sma">SMA</option>
                            <option value="s1">S1</option>';
                        } else if($value->pendidikan == 'smp') {
                            echo '
                            <option value="sd">SD</option>
                            <option selected value="smp">SMP</option>
                            <option value="sma">SMA</option>
                            <option value="s1">S1</option>';
                        } else if($value->pendidikan == 'sma') {
                            echo '
                            <option value="sd">SD</option>
                            <option value="smp">SMP</option>
                            <option selected value="sma">SMA</option>
                            <option value="s1">S1</option>';
                        } else if($value->pendidikan == 's1') {
                            echo '
                            <option value="sd">SD</option>
                            <option value="smp">SMP</option>
                            <option value="sma">SMA</option>
                            <option selected value="s1">S1</option>';
                        }
                    ?>
                    </select>
                  </div>
                  <div class="input-group mb-3">
                    <select name="jenis_kelamin" class="custom-select" id="inputGroupSelect01">
                    <?php 
                        if($value->jenis_kelamin == 'laki-laki') {
                            echo '
                            <option selected value="laki-laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>';
                        } else if($value->jenis_kelamin == 'perempuan') {
                            echo '
                            <option value="laki-laki">Laki-laki</option>
                            <option selected value="perempuan">Perempuan</option>';
                        }
                    ?>
                    </select>
                  </div>
                  <div class="input-group mb-3">
                    <button type="submit" class="btn btn-primary">Edit</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </form>
        <?php } ?>
        </div>
      </div>

    <?php $this->load->view('layout/footer') ?>      

    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php $this->load->view('layout/js') ?>
  
</body>
</html>